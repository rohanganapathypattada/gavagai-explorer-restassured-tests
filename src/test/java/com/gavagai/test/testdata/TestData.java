package com.gavagai.test.testdata;

import java.io.InputStream;
import java.util.Properties;

/**
 * This class handles the centralization of testData under the
 * resounce/TestData/testData.properties file
 * 
 * @author rohan
 *
 */
public class TestData {
	
	public static Properties prop = new Properties();
	public static InputStream File;
	public void loadData() throws Exception {
		File = TestData.class.getClassLoader().getResourceAsStream("TestData/testData.properties");
		prop.load(File);
	}
	
	public String getXLSXTESTDATAPATH() throws Exception {
		loadData();
		return prop.getProperty("XLSXTESTDATAPATH");		
	}
	
	public String getXLSTESTDATAPATH() throws Exception {
		loadData();
		return prop.getProperty("XLSTESTDATAPATH");		
	}
	
	public String getCSVTESTDATAPATH() throws Exception {
		loadData();
		return prop.getProperty("CSVTESTDATAPATH");		
	}
	
	public String getGENERICTESTDATAPATH() throws Exception {
		loadData();
		return prop.getProperty("GENERICTESTDATAPATH");		
	}

}
