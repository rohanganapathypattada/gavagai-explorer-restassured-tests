package com.gavagai.test.testdata;

import java.io.InputStream;
import java.util.Properties;

/**
 * This class handles the centralization of FileTypeTestsData under the
 * resounce/TestData/FileTypeTestsData.properties file
 * 
 * @author rohan
 *
 */
public class FileTypeTestsData {
	
	public static Properties prop = new Properties();
	public static InputStream File;
	public void loadData() throws Exception {
		File = FileTypeTestsData.class.getClassLoader().getResourceAsStream("TestData/fileTypeTestsData.properties");
		prop.load(File);
	}
	
	public String getAPIAccessible() throws Exception {
		loadData();
		return prop.getProperty("APIAccessible");		
	}
	
	public String getemptyFile() throws Exception {
		loadData();
		return prop.getProperty("emptyFile");		
	}
	
	public String getEmpty1stRow() throws Exception {
		loadData();
		return prop.getProperty("Empty1stRow");		
	}
	
	public String getnumbersIn1stRow() throws Exception {
		loadData();
		return prop.getProperty("numbersIn1stRow");		
	}
	
	public String getmergedCellsIn1stRow() throws Exception {
		loadData();
		return prop.getProperty("mergedCellsIn1stRow");		
	}
	
	public String getnewLineCharacter() throws Exception {
		loadData();
		return prop.getProperty("newLineCharacter");		
	}
	
	public String getdifferentFontsAndfontSizes() throws Exception {
		loadData();
		return prop.getProperty("differentFontsAndfontSizes");		
	}
	
	public String gettextColourAndCellColour() throws Exception {
		loadData();
		return prop.getProperty("textColourAndCellColour");		
	}
	
	public String getsecondRowMoreContentThanFirstRow() throws Exception {
		loadData();
		return prop.getProperty("secondRowMoreContentThanFirstRow");		
	}
	
	public String getcellFormats() throws Exception {
		loadData();
		return prop.getProperty("cellFormats");		
	}
	
	public String getspecialCharacters() throws Exception {
		loadData();
		return prop.getProperty("specialCharacters");		
	}
	
	public String getemptyCellsBetweenRows() throws Exception {
		loadData();
		return prop.getProperty("emptyCellsBetweenRows");		
	}
	
	public String getfileWithOneCell() throws Exception {
		loadData();
		return prop.getProperty("fileWithOneCel");		
	}
	
	public String getmaximumRows() throws Exception {
		loadData();
		return prop.getProperty("maximumRows");		
	}
	
	public String getstressTestMaximumRows() throws Exception {
		loadData();
		return prop.getProperty("stressTestMaximumRows");		
	}
	
	public String getmaximumColumns() throws Exception {
		loadData();
		return prop.getProperty("maximumColumns");		
	}
	
	public String getpasswordFile() throws Exception {
		loadData();
		return prop.getProperty("passwordFile");		
	}
	
	public String getencodingParameter() throws Exception {
		loadData();
		return prop.getProperty("encodingParameter");		
	}
	
	public String getmaximumColumnsCSV() throws Exception {
		loadData();
		return prop.getProperty("maximumColumnsCSV");		
	}
	
	public String getmaximumColumnsXLS() throws Exception {
		loadData();
		return prop.getProperty("maximumColumnsXLS");		
	}
	
	public String getcellFormatsCSV() throws Exception {
		loadData();
		return prop.getProperty("cellFormatsCSV");		
	}

}
