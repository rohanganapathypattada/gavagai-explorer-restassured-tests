package com.gavagai.test.testdata;

import java.io.InputStream;
import java.util.Properties;

/**
 * This class handles the centralization of GenericTestsData under the
 * resounce/TestData/GenericTestsData.properties file
 * 
 * @author rohan
 *
 */
public class GenericTestsData {
	
	public static Properties prop = new Properties();
	public static InputStream File;
	public void loadData() throws Exception {
		File = GenericTestsData.class.getClassLoader().getResourceAsStream("TestData/genericTestsData.properties");
		prop.load(File);
	}
	
	public String getAPIAccessible() throws Exception {
		loadData();
		return prop.getProperty("APIAccessible");		
	}
	
	public String getresponseHeaderKeyValue() throws Exception {
		loadData();
		return prop.getProperty("responseHeaderKeyValue");		
	}
	
	public String getunsupportedFileFormat() throws Exception {
		loadData();
		return prop.getProperty("unsupportedFileFormat");		
	}
	
	public String getfileParameterMissing() throws Exception {
		loadData();
		return prop.getProperty("fileParameterMissing");		
	}
	
	public String getlargeFileSize() throws Exception {
		loadData();
		return prop.getProperty("largeFileSize");		
	}
	
	public String getdifferentResponseType() throws Exception {
		loadData();
		return prop.getProperty("differentResponseType");		
	}

}
