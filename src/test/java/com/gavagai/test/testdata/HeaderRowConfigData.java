package com.gavagai.test.testdata;

import java.io.InputStream;
import java.util.Properties;

/**
 * header row page related API Config Data file To refer to API end points and URLs
 * from the resource/TestData/headerRow.properties file
 * 
 * @author rohan
 *
 */

public class HeaderRowConfigData {

	public static Properties prop = new Properties();
	public static InputStream File;

	public void loadData() throws Exception {
		File = HeaderRowConfigData.class.getClassLoader().getResourceAsStream("TestData/headerRow.properties");
		prop.load(File);
	}

	public String getAPIName() throws Exception {
		loadData();
		return prop.getProperty("APINAME");
	}

	public String getAPIEncoding_UTF8() throws Exception {
		loadData();
		return prop.getProperty("APIENCODING_UTF8");
	}

	public String getAPIEncoding_UTF16() throws Exception {
		loadData();
		return prop.getProperty("APIENCODING_UTF16");
	}
	
	public String getAPIEncoding_UTF32() throws Exception {
		loadData();
		return prop.getProperty("APIENCODING_UTF32");
	}

}
