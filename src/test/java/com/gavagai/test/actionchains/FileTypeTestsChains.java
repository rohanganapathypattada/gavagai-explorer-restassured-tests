package com.gavagai.test.actionchains;

import static org.assertj.core.api.Assertions.assertThat;

import com.gavagai.test.config.ConfigData;
import com.gavagai.test.testdata.FileTypeTestsData;
import com.gavagai.test.testdata.HeaderRowConfigData;
import com.gavagai.test.utils.Library;

import io.restassured.response.Response;

public class FileTypeTestsChains {

	public Response response;
	public String fileName;
	public int statusCode;
	public int responseCode;
	public String responseBody;
	public String responseHeader;
	public String testCase;

	Library library = new Library();
	ConfigData configData = new ConfigData();
	FileTypeTestsData fileTypeTestsData = new FileTypeTestsData();
	HeaderRowConfigData headerRowConfigData = new HeaderRowConfigData();
	CommonChains commonChains = new CommonChains();

	public Response accessAPI(String fileName, String testCase) throws Throwable {

		this.fileName = fileName;

		response = library.getResponse(configData.getAPILink(), configData.getAuthToken(), configData.getContentType(),
				configData.getAccept(), commonChains.getTestDataPath(testCase) + fileName, configData.getPostLink(),
				headerRowConfigData.getAPIName());

		return response;

	}

	public void assertResponses(String responseType, String fileName, Response response) throws Exception {

		String fileFormat = getFileFormat(fileName);

		if (fileName.contentEquals("API_Accessible" + fileFormat)) {

			if (library.getResponseCode(response) == 200) {

				assertThat(library.getResponseAsString(response))
						.as("Verify if the response " + responseType + " is correct, returned : "
								+ library.getResponseAsString(response))
						.isEqualTo(fileTypeTestsData.getAPIAccessible());

			}

		} else if (fileName.contentEquals("Empty" + fileFormat)) {

			assertThat(library.getResponseAsString(response)).as("Verify if the response " + responseType
					+ " is correct, returned : " + library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getemptyFile());

		} else if (fileName.contentEquals("CellFormats" + fileFormat)) {

			if (fileFormat.contains(".xlsx") || fileFormat.contains(".xls")) {

				assertThat(library.getResponseAsString(response)).as("Verify if the response " + responseType
						+ " is correct, returned : " + library.getResponseAsString(response))
						.isEqualTo(fileTypeTestsData.getcellFormats());

			} else if (fileFormat.contains(".csv")) {

				assertThat(library.getResponseAsString(response))
						.as("Verify if the response " + responseType + " is correct, returned : "
								+ library.getResponseAsString(response))
						.isEqualTo(fileTypeTestsData.getcellFormatsCSV());

			}

		} else if (fileName.contentEquals("EmptyCellsBetweenRows" + fileFormat)) {

			assertThat(library.getResponseAsString(response))
					.as("Verify if the response " + responseType + " is correct, returned : "
							+ library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getemptyCellsBetweenRows());

		} else if (fileName.contentEquals("EmptyFirstRow" + fileFormat)) {

			assertThat(library.getResponseAsString(response)).as("Verify if the response " + responseType
					+ " is correct, returned : " + library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getEmpty1stRow());

		} else if (fileName.contentEquals("EncodingParameter" + fileFormat)) {

			assertThat(library.getResponseAsString(response))
					.as("Verify if the response " + responseType + " is correct, returned : "
							+ library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getencodingParameter());

		} else if (fileName.contentEquals("FontsAndFontSize" + fileFormat)) {

			assertThat(library.getResponseAsString(response))
					.as("Verify if the response " + responseType + " is correct, returned : "
							+ library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getdifferentFontsAndfontSizes());

		} else if (fileName.contentEquals("MaximumRows" + fileFormat)) {

			assertThat(library.getResponseAsString(response)).as("Verify if the response " + responseType
					+ " is correct, returned : " + library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getmaximumRows());

		} else if (fileName.contentEquals("MaxiumumCol" + fileFormat)) {

			if (fileFormat.contains(".xlsx")) {

				assertThat(library.getResponseAsString(response))
						.as("Verify if the response " + responseType + " is correct, returned : "
								+ library.getResponseAsString(response))
						.isEqualTo(fileTypeTestsData.getmaximumColumns());

			} else if (fileFormat.contains(".csv")) {

				assertThat(library.getResponseAsString(response))
						.as("Verify if the response " + responseType + " is correct, returned : "
								+ library.getResponseAsString(response))
						.isEqualTo(fileTypeTestsData.getmaximumColumnsCSV());

			} else if (fileFormat.contains(".xls")) {

				assertThat(library.getResponseAsString(response))
						.as("Verify if the response " + responseType + " is correct, returned : "
								+ library.getResponseAsString(response))
						.isEqualTo(fileTypeTestsData.getmaximumColumnsXLS());

			}

		} else if (fileName.contentEquals("MergedCells" + fileFormat)) {

			assertThat(library.getResponseAsString(response))
					.as("Verify if the response " + responseType + " is correct, returned : "
							+ library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getmergedCellsIn1stRow());

		} else if (fileName.contentEquals("NewLine" + fileFormat)) {

			assertThat(library.getResponseAsString(response)).as("Verify if the response " + responseType
					+ " is correct, returned : " + library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getnewLineCharacter());

		} else if (fileName.contentEquals("Numbers" + fileFormat)) {

			assertThat(library.getResponseAsString(response)).as("Verify if the response " + responseType
					+ " is correct, returned : " + library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getnumbersIn1stRow());

		} else if (fileName.contentEquals("OneCellHide" + fileFormat)) {

			assertThat(library.getResponseAsString(response)).as("Verify if the response " + responseType
					+ " is correct, returned : " + library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getfileWithOneCell());

		} else if (fileName.contentEquals("password" + fileFormat)) {

			assertThat(library.getResponseAsString(response)).as("Verify if the response " + responseType
					+ " is correct, returned : " + library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getpasswordFile());

		} else if (fileName.contentEquals("SecondRowMoreContent" + fileFormat)) {

			assertThat(library.getResponseAsString(response))
					.as("Verify if the response " + responseType + " is correct, returned : "
							+ library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getsecondRowMoreContentThanFirstRow());

		} else if (fileName.contentEquals("SpecialCharacters" + fileFormat)) {

			assertThat(library.getResponseAsString(response))
					.as("Verify if the response " + responseType + " is correct, returned : "
							+ library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getspecialCharacters());

		} else if (fileName.contentEquals("StressTestMaximumRows" + fileFormat)) {

			assertThat(library.getResponseAsString(response))
					.as("Verify if the response " + responseType + " is correct, returned : "
							+ library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.getstressTestMaximumRows());

		} else if (fileName.contentEquals("TextColourAndCellColour" + fileFormat)) {

			assertThat(library.getResponseAsString(response))
					.as("Verify if the response " + responseType + " is correct, returned : "
							+ library.getResponseAsString(response))
					.isEqualTo(fileTypeTestsData.gettextColourAndCellColour());

		}

	}

	protected String getFileFormat(String fileName) {

		String fileFormat = null;

		if (fileName.contains(".xlsx")) {

			fileFormat = ".xlsx";

		} else if (fileName.contains(".csv")) {

			fileFormat = ".csv";

		} else if (fileName.contains(".xls")) {

			fileFormat = ".xls";

		}

		return fileFormat;

	}

}
