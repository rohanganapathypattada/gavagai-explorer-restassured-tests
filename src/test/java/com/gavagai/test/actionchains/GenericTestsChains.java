package com.gavagai.test.actionchains;

import static org.assertj.core.api.Assertions.assertThat;

import com.gavagai.test.config.ConfigData;
import com.gavagai.test.testdata.GenericTestsData;
import com.gavagai.test.testdata.HeaderRowConfigData;
import com.gavagai.test.testdata.TestData;
import com.gavagai.test.utils.Library;

import io.restassured.response.Response;

public class GenericTestsChains {

	public Response response;
	public String fileName;
	public int statusCode;
	public int responseCode;
	public String responseBody;
	public String responseHeader;

	Library library = new Library();
	TestData testData = new TestData();
	ConfigData configData = new ConfigData();
	GenericTestsData genericTestsData = new GenericTestsData();
	HeaderRowConfigData headerRowConfigData = new HeaderRowConfigData();

	public Response accessAPI(String fileName) throws Throwable {

		this.fileName = fileName;

		response = library.getResponse(configData.getAPILink(), configData.getAuthToken(), configData.getContentType(),
				configData.getAccept(), testData.getGENERICTESTDATAPATH() + fileName, configData.getPostLink(),
				headerRowConfigData.getAPIName());

		return response;

	}

	public Response accessAPIWrongToken(String fileName) throws Throwable {

		this.fileName = fileName;

		response = library.getResponse(configData.getAPILink(), configData.getAuthToken() + "WrongToken",
				configData.getContentType(), configData.getAccept(), testData.getGENERICTESTDATAPATH() + this.fileName,
				configData.getPostLink(), headerRowConfigData.getAPIName());

		return response;

	}

	public Response accessAPINoFile() throws Throwable {

		response = library.getResponse(configData.getAPILink(), configData.getAuthToken(), configData.getContentType(),
				configData.getAccept(), configData.getPostLink(), headerRowConfigData.getAPIName());

		return response;

	}

	public Response accessAPIDiffResponseType(String fileName) throws Throwable {

		this.fileName = fileName;

		response = library.getResponse(configData.getAPILink(), configData.getAuthToken(), configData.getContentType(),
				configData.getAcceptXML(), testData.getGENERICTESTDATAPATH() + fileName, configData.getPostLink(),
				headerRowConfigData.getAPIName());

		return response;

	}

	public void assertResponses(String responseType, String fileName, Response response) throws Exception {

		if (responseType.contentEquals("Header")) {

			assertThat(library.getResponseHeaders(response))
					.as("Verify if the response " + responseType + " is correct, returned : "
							+ library.getResponseHeaders(response))
					.contains(genericTestsData.getresponseHeaderKeyValue());

		} else if (responseType.contentEquals("Body")) {

			if (fileName != null) {

				if (fileName.contentEquals("API_Accessible.xlsx")) {

					if (library.getResponseCode(response) == 200) {

						assertThat(library.getResponseAsString(response))
								.as("Verify if the response " + responseType + " is correct, returned : "
										+ library.getResponseAsString(response))
								.isEqualTo(genericTestsData.getAPIAccessible());

					} else if (library.getResponseCode(response) == 406) {

						assertThat(library.getResponseAsString(response))
								.as("Verify if the response " + responseType + " is correct, returned : "
										+ library.getResponseAsString(response))
								.contains(genericTestsData.getdifferentResponseType());

					}

				} else if (fileName.contentEquals("testData.txt")) {

					assertThat(library.getResponseAsString(response))
							.as("Verify if the response " + responseType + " is correct, returned : "
									+ library.getResponseAsString(response))
							.isEqualTo(genericTestsData.getunsupportedFileFormat());

				} else if (fileName.contentEquals("MaxLargeFileSize.xlsx")) {

					assertThat(library.getResponseAsString(response))
							.as("Verify if the response " + responseType + " is correct, returned : "
									+ library.getResponseAsString(response))
							.contains(genericTestsData.getlargeFileSize());

				}

			} else {

				assertThat(library.getResponseAsString(response))
						.as("Verify if the response " + responseType + " is correct, returned : "
								+ library.getResponseAsString(response))
						.isEqualTo(genericTestsData.getfileParameterMissing());

			}

		}
	}

}
