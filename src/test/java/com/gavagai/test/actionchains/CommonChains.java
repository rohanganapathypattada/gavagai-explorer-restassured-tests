package com.gavagai.test.actionchains;

import com.gavagai.test.config.ConfigData;
import com.gavagai.test.testdata.HeaderRowConfigData;
import com.gavagai.test.testdata.TestData;
import com.gavagai.test.utils.Library;

import io.restassured.response.Response;

public class CommonChains {

	public Response response;
	public String fileName;

	Library library = new Library();
	ConfigData configData = new ConfigData();
	static GenericTestsChains genericTestsChains = new GenericTestsChains();
	static FileTypeTestsChains fileTypeTestsChains = new FileTypeTestsChains();
	static HeaderRowConfigData headerRowConfigData = new HeaderRowConfigData();
	static TestData testData = new TestData();

	public Response accessAPIWithEncoding(String fileName, String encodingValue, String testCase) throws Throwable {

		this.fileName = fileName;

		response = library.getResponse(configData.getAPILink(), configData.getAuthToken(), configData.getContentType(),
				configData.getAccept(), getTestDataPath(testCase) + fileName, configData.getPostLink(),
				configData.getEncoding() + getEncodingValue(encodingValue), headerRowConfigData.getAPIName());

		return response;

	}

	public static Response getResponse(String fileName, String testCases) throws Throwable {

		Response responseType = null;

		switch (testCases) {
		case "Generic":
			responseType = genericTestsChains.accessAPI(fileName);
			break;
		case "XLSX":
			responseType = fileTypeTestsChains.accessAPI(fileName, testCases);
			break;
		case "CSV":
			responseType = fileTypeTestsChains.accessAPI(fileName, testCases);
			break;
		case "XLS":
			responseType = fileTypeTestsChains.accessAPI(fileName, testCases);
			break;
		}

		return responseType;

	}

	public void assertResponses(String testCases, String responseType, String fileName, Response response)
			throws Throwable {

		switch (testCases) {
		case "Generic":
			genericTestsChains.assertResponses(responseType, fileName, response);
			break;
		case "XLSX":
			fileTypeTestsChains.assertResponses(responseType, fileName, response);
			break;
		case "CSV":
			fileTypeTestsChains.assertResponses(responseType, fileName, response);
			break;
		case "XLS":
			fileTypeTestsChains.assertResponses(responseType, fileName, response);
			break;
		}

	}

	public String getTestDataPath(String testCases) throws Throwable {

		String testDataPath = null;

		switch (testCases) {
		case "Generic":
			testDataPath = testData.getGENERICTESTDATAPATH();
			break;
		case "XLSX":
			testDataPath = testData.getXLSXTESTDATAPATH();
			break;
		case "CSV":
			testDataPath = testData.getCSVTESTDATAPATH();
			break;
		case "XLS":
			testDataPath = testData.getXLSTESTDATAPATH();
			break;
		}

		return testDataPath;

	}

	public String getEncodingValue(String encodingValue) throws Throwable {

		String getEncodingValue = null;

		switch (encodingValue) {
		case "UTF-8":
			getEncodingValue = headerRowConfigData.getAPIEncoding_UTF8();
			break;
		case "UTF-16":
			getEncodingValue = headerRowConfigData.getAPIEncoding_UTF16();
			break;
		case "UTF-32":
			getEncodingValue = headerRowConfigData.getAPIEncoding_UTF32();
			break;

		}

		return getEncodingValue;

	}

}
