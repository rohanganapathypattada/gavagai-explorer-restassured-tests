package com.gavagai.test.config;

import java.io.InputStream;
import java.util.Properties;

/**
 * This class handles the centralization of all configurations of the framework
 * under the resource/config.properties file
 * 
 * @author rohan
 *
 */

public class ConfigData {

	public static Properties prop = new Properties();
	public static InputStream File;

	public void loadData() throws Exception {
		File = ConfigData.class.getClassLoader().getResourceAsStream("config.properties");
		prop.load(File);
	}

	public String getAPILink() throws Exception {
		loadData();
		return prop.getProperty("APILINK");
	}

	public String getAuthToken() throws Exception {
		loadData();
		return prop.getProperty("AUTHTOKEN");
	}

	public String getContentType() throws Exception {
		loadData();
		return prop.getProperty("CONTENTTYPE");
	}

	public String getAccept() throws Exception {
		loadData();
		return prop.getProperty("ACCEPT");
	}
	
	public String getPostLink() throws Exception {
		loadData();
		return prop.getProperty("POSTLINK");
	}
	
	public String getAcceptXML() throws Exception {
		loadData();
		return prop.getProperty("ACCEPTXML");
	}
	
	public String getEncoding() throws Exception {
		loadData();
		return prop.getProperty("ENCODING");
	}

}
