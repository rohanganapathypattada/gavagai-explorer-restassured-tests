package com.gavagai.test.utils;

import static io.restassured.RestAssured.given;

import java.io.File;
import io.restassured.RestAssured;
import io.restassured.response.Response;

/**
 * Bunch of common utils that can be used through out the framework
 * @author Rohan Ganapathy
 *
 */
public class Library {
	
	public Response getResponse(String URL, String authToken, String contentType, String accept, String file, String post, String APIName) {
		
		RestAssured.baseURI = URL;
		
		Response response=given()
		.header("Authorization", "Bearer "+authToken)
		.header("Content-Type", contentType)
		.header("Accept", accept)
		.multiPart("file",new File(file))
		.when().post(post+APIName);
		
		return response;
	}
	
	public Response getResponse(String URL, String authToken, String contentType, String accept, String post, String APIName) {
		
		RestAssured.baseURI = URL;
		
		Response response=given()
		.header("Authorization", "Bearer "+authToken)
		.header("Content-Type", contentType)
		.header("Accept", accept)
		.when().post(post+APIName);
		
		return response;
	}
	
	public Response getResponse(String URL, String authToken, String contentType, String accept, String file, String post,String encoding, String APIName) {
		
		RestAssured.baseURI = URL;
		
		Response response=given()
		.header("Authorization", "Bearer "+authToken)
		.header("Content-Type", contentType)
		.header("Accept", accept)
		.multiPart("file",new File(file))
		.when().post(post+APIName+encoding);
		
		return response;
	}
	
	public String getResponseAsString(Response response) {
		
		return response.then().extract().response().asString();
		
	}
	
	public String getResponseHeaders(Response response) {
		
		return response.then().extract().response().getHeaders().asList().toString();
		
	}
	
	public int getResponseCode(Response response) {
		
		return response.then().extract().response().statusCode();
		
	}

}
