package com.gavagai.test.mainrunner;

import com.gavagai.test.actionchains.CommonChains;
import com.gavagai.test.actionchains.FileTypeTestsChains;
import com.gavagai.test.actionchains.GenericTestsChains;
import com.gavagai.test.utils.Library;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.response.Response;
import static org.assertj.core.api.Assertions.*;

public class gavagai_Main {

	public Response response;
	public String fileName;
	public int responseCode;
	public static String testCases;

	Library library = new Library();
	CommonChains commonChains = new CommonChains();
	GenericTestsChains genericTestsChains = new GenericTestsChains();
	FileTypeTestsChains fileTypeTestsChains = new FileTypeTestsChains();

	@Given("^\"([^\"]*)\" Test Cases$")
	public void test_Cases(String testCasesName) throws Throwable {
		testCases = testCasesName;
	}

	@Given("^Acccess the header_row API using the \"([^\"]*)\" file$")
	public void acccess_the_header_row_API_using_the_file(String fileName) throws Throwable {

		this.fileName = fileName;

		this.response = commonChains.getResponse(fileName, testCases);

	}

	@Then("^Verify if the response returns the correct status code \"([^\"]*)\"$")
	public void verify_if_the_response_returns_the_correct_status_code(int statusCode) throws Throwable {
		assertThat(library.getResponseCode(this.response))
				.as("Verify if the response returns the correct status code, returned : "
						+ library.getResponseCode(this.response))
				.isEqualTo(statusCode);
	}

	@And("^Verify if the response \"([^\"]*)\" is correct$")
	public void verify_if_the_response_is_correct(String responseType) throws Throwable {

		commonChains.assertResponses(testCases, responseType, this.fileName, this.response);

	}

	@Given("^Acccess the header_row API using the \"([^\"]*)\" file with wrong token$")
	public void acccess_the_header_row_API_using_the_file_with_wrong_token(String fileName) throws Throwable {

		this.fileName = fileName;

		this.response = genericTestsChains.accessAPIWrongToken(fileName);
	}

	@Given("^Acccess the header_row API using the with no file$")
	public void acccess_the_header_row_API_using_the_with_no_file() throws Throwable {

		this.response = genericTestsChains.accessAPINoFile();

	}

	@Given("^Acccess the header_row API using the \"([^\"]*)\" file with encoding parameter \"([^\"]*)\"$")
	public void acccess_the_header_row_API_using_the_file_with_encoding_parameter(String fileName, String encodingValue)
			throws Throwable {

		this.fileName = fileName;

		this.response = commonChains.accessAPIWithEncoding(fileName, encodingValue, testCases);

	}

	@Given("^Acccess the header_row API using the \"([^\"]*)\" file with different response type$")
	public void acccess_the_header_row_API_using_the_file_with_different_response_type(String fileName)
			throws Throwable {

		this.fileName = fileName;

		this.response = genericTestsChains.accessAPIDiffResponseType(fileName);

	}

}