Feature: Test header_row Gavagai Explorer API.

  #=============================== Generic tests ===============================
  Scenario Outline: GENERIC: "<testScenario>"
    Given "Generic" Test Cases
    Given Acccess the header_row API using the "<fileName>" file
    Then Verify if the response returns the correct status code "<statusCode>"
    And Verify if the response "<responseValue>" is correct

    Examples: 
      | testScenario                                                                   | fileName              | statusCode | responseValue |
      | Test to Check API is accessible                                                | API_Accessible.xlsx   |        200 | Body          |
      | Test to Check if API Response Header have the correct following Key:Value pair | API_Accessible.xlsx   |        200 | Header        |
      | Test to Check API response for File format other than xlsx, xls or csv         | testData.txt          |        400 | Body          |
      | Test to Check when file size is more than maximum allowed file size (107MB).   | MaxLargeFileSize.xlsx |        413 | Body          |

  Scenario: GENERIC: Test to Check API response when wrong token is Provided for Authorization
    Given Acccess the header_row API using the "API_Accessible.xlsx" file with wrong token
    Then Verify if the response returns the correct status code "401"

  Scenario: GENERIC: Test to Check API response for No File uploaded
    Given Acccess the header_row API using the with no file
    Then Verify if the response returns the correct status code "400"

  Scenario: GENERIC: Test to Check API response when the file parameter is removed/missing.
    Given Acccess the header_row API using the with no file
    Then Verify if the response returns the correct status code "400"
    And Verify if the response "Body" is correct

  Scenario: GENERIC: Test to Check if API Response is correct when encoding parameter is passed (UTF-8)
    Given Acccess the header_row API using the "API_Accessible.xlsx" file with encoding parameter "UTF-8"
    Then Verify if the response returns the correct status code "200"
    And Verify if the response "Body" is correct

  Scenario: GENERIC: Test to check if Request header accepts anyother response type other than JSON
    Given Acccess the header_row API using the "API_Accessible.xlsx" file with different response type
    Then Verify if the response returns the correct status code "406"
    And Verify if the response "Body" is correct

  #=============================== XLSX tests ===============================
  Scenario Outline: XLSX: "<testScenario>"
    Given "XLSX" Test Cases
    Given Acccess the header_row API using the "<fileName>" file
    Then Verify if the response returns the correct status code "<statusCode>"
    And Verify if the response "<responseValue>" is correct

    Examples: 
      | testScenario                                                                                                              | fileName                     | statusCode | responseValue |
      | Test to check when a Empty file is uploaded                                                                               | Empty.xlsx                   |        500 | Body          |
      | Test to Check if first row content is retrieved                                                                           | API_Accessible.xlsx          |        200 | Body          |
      | Test to Check if first row content has a Number                                                                           | Numbers.xlsx                 |        200 | Body          |
      | Test to Check if first row content is empty with contents present in 2nd row                                              | EmptyFirstRow.xlsx           |        200 | Body          |
      | Test to Check Merged cells in 1st row are displayed correctly                                                             | MergedCells.xlsx             |        200 | Body          |
      | Test to check if New line charecter in a single cells in 1st row are displayed correctly                                  | NewLine.xlsx                 |        200 | Body          |
      | Test to check text in different Fonts and Font Sizes in 1st row are displayed correctly                                   | FontsAndFontSize.xlsx        |        200 | Body          |
      | Test to check Font Color or Cells with Color in 1st row are displayed correctly                                           | TextColourAndCellColour.xlsx |        200 | Body          |
      | Test to check if 2nd row has more filled rows than 1st row                                                                | SecondRowMoreContent.xlsx    |        200 | Body          |
      | Test to check if the 1st row cell format is different (eg: Number, Currency, Date, Time)                                  | CellFormats.xlsx             |        200 | Body          |
      | Test to check if the 1st row has special characters                                                                       | SpecialCharacters.xlsx       |        200 | Body          |
      | Test to check if Empty cells between entries in 1st row are shown correctly(make sure to have concurrent empty cells too) | EmptyCellsBetweenRows.xlsx   |        200 | Body          |
      | Test to check a file with only one cell that's empty                                                                      | OneCellHide.xlsx             |        200 | Body          |
      | Test to Check max supported row (250,000)                                                                                 | MaximumRows.xlsx             |        200 | Body          |
      | Test to Check more than max supported rows                                                                                | StressTestMaximumRows.xlsx   |        200 | Body          |
      | Test to Check more than max supported columns (max 16,384)                                                                | MaxiumumCol.xlsx             |        200 | Body          |
      | Test to Check a xlsx file with password                                                                                   | password.xlsx                |        400 | Body          |

  Scenario Outline: XLSX: Test to Check if API Response is correct when encoding parameter is passed with "<encodingValue>"
    Given Acccess the header_row API using the "<fileName>" file with encoding parameter "<encodingValue>"
    Then Verify if the response returns the correct status code "<statusCode>"
    And Verify if the response "<responseValue>" is correct

    Examples: 
      | encodingValue | fileName               | statusCode | responseValue |
      | UTF-8         | EncodingParameter.xlsx |        200 | Body          |
      | UTF-16        | EncodingParameter.xlsx |        200 | Body          |
      | UTF-32        | EncodingParameter.xlsx |        200 | Body          |
      | Test          | EncodingParameter.xlsx |        200 | Body          |

  #=============================== CSV tests ===============================
  Scenario Outline: CSV: "<testScenario>"
    Given "CSV" Test Cases
    Given Acccess the header_row API using the "<fileName>" file
    Then Verify if the response returns the correct status code "<statusCode>"
    And Verify if the response "<responseValue>" is correct

    Examples: 
      | testScenario                                                                                                              | fileName                  | statusCode | responseValue |
      | Test to check when a Empty file is uploaded                                                                               | Empty.csv                 |        500 | Body          |
      | Test to Check if first row content is retrieved                                                                           | API_Accessible.csv        |        200 | Body          |
      | Test to Check if first row content has a Number                                                                           | Numbers.csv               |        200 | Body          |
      | Test to Check if first row content is empty with contents present in 2nd row                                              | EmptyFirstRow.csv         |        200 | Body          |
      | Test to Check Merged cells in 1st row are displayed correctly                                                             | MergedCells.csv           |        200 | Body          |
      | Test to check if New line charecter in a single cells in 1st row are displayed correctly                                  | NewLine.csv               |        200 | Body          |
      | Test to check if 2nd row has more filled rows than 1st row                                                                | SecondRowMoreContent.csv  |        200 | Body          |
      | Test to check if the 1st row cell format is different (eg: Number, Currency, Date, Time)                                  | CellFormats.csv           |        200 | Body          |
      | Test to check if the 1st row has special characters                                                                       | SpecialCharacters.csv     |        200 | Body          |
      | Test to check if Empty cells between entries in 1st row are shown correctly(make sure to have concurrent empty cells too) | EmptyCellsBetweenRows.csv |        200 | Body          |
      | Test to Check max supported row (250,000)                                                                                 | MaximumRows.csv           |        200 | Body          |
      | Test to Check more than max supported rows                                                                                | StressTestMaximumRows.csv |        200 | Body          |
      | Test to Check more than max supported columns (max 256)                                                                   | MaxiumumCol.csv           |        200 | Body          |

  Scenario Outline: CSV: Test to Check if API Response is correct when encoding parameter is passed with "<encodingValue>"
    Given Acccess the header_row API using the "<fileName>" file with encoding parameter "<encodingValue>"
    Then Verify if the response returns the correct status code "<statusCode>"
    And Verify if the response "<responseValue>" is correct

    Examples: 
      | encodingValue | fileName              | statusCode | responseValue |
      | UTF-8         | EncodingParameter.csv |        200 | Body          |
      | UTF-16        | EncodingParameter.csv |        200 | Body          |
      | UTF-32        | EncodingParameter.csv |        200 | Body          |
      | Test          | EncodingParameter.csv |        200 | Body          |

  #=============================== xls tests ===============================
  Scenario Outline: XLS: "<testScenario>"
    Given "XLS" Test Cases
    Given Acccess the header_row API using the "<fileName>" file
    Then Verify if the response returns the correct status code "<statusCode>"
    And Verify if the response "<responseValue>" is correct

    Examples: 
      | testScenario                                                                                                              | fileName                    | statusCode | responseValue |
      | Test to check when a Empty file is uploaded                                                                               | Empty.xls                   |        500 | Body          |
      | Test to Check if first row content is retrieved                                                                           | API_Accessible.xls          |        200 | Body          |
      | Test to Check if first row content has a Number                                                                           | Numbers.xls                 |        200 | Body          |
      | Test to Check if first row content is empty with contents present in 2nd row                                              | EmptyFirstRow.xls           |        200 | Body          |
      | Test to Check Merged cells in 1st row are displayed correctly                                                             | MergedCells.xls             |        200 | Body          |
      | Test to check if New line charecter in a single cells in 1st row are displayed correctly                                  | NewLine.xls                 |        200 | Body          |
      | Test to check text in different Fonts and Font Sizes in 1st row are displayed correctly                                   | FontsAndFontSize.xls        |        200 | Body          |
      | Test to check Font Color or Cells with Color in 1st row are displayed correctly                                           | TextColourAndCellColour.xls |        200 | Body          |
      | Test to check if 2nd row has more filled rows than 1st row                                                                | SecondRowMoreContent.xls    |        200 | Body          |
      | Test to check if the 1st row cell format is different (eg: Number, Currency, Date, Time)                                  | CellFormats.xls             |        200 | Body          |
      | Test to check if the 1st row has special characters                                                                       | SpecialCharacters.xls       |        200 | Body          |
      | Test to check if Empty cells between entries in 1st row are shown correctly(make sure to have concurrent empty cells too) | EmptyCellsBetweenRows.xls   |        200 | Body          |
      | Test to check a file with only one cell that's empty                                                                      | OneCellHide.xls             |        200 | Body          |
      | Test to Check max supported row (65,536)                                                                                  | MaximumRows.xls             |        200 | Body          |
      | Test to Check more than max supported columns (max 256)                                                                   | MaxiumumCol.xls             |        200 | Body          |
      | Test to Check a xls file with password                                                                                    | password.xls                |        400 | Body          |

  Scenario Outline: XLS: Test to Check if API Response is correct when encoding parameter is passed with "<encodingValue>"
    Given Acccess the header_row API using the "<fileName>" file with encoding parameter "<encodingValue>"
    Then Verify if the response returns the correct status code "<statusCode>"
    And Verify if the response "<responseValue>" is correct

    Examples: 
      | encodingValue | fileName              | statusCode | responseValue |
      | UTF-8         | EncodingParameter.xls |        200 | Body          |
      | UTF-16        | EncodingParameter.xls |        200 | Body          |
      | UTF-32        | EncodingParameter.xls |        200 | Body          |
      | Test          | EncodingParameter.xls |        200 | Body          |
