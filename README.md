# Gavagai Explorer REST-Assured Tests
> Gavagai Explorer API Tests using REST-Assured with Cucumber-TestNG Hybrid framework

[![java Version][java-image]][java-url]
[![mvn Version][mvn-image]][mvn-url]
[![cucumber Version][cucumber-image]][cucumber-url]
[![testng Version][testng-image]][testng-url]
[![restassured Version][restassured-image]][restassured-url]

Gavagai Explorer API Tests use REST-Assured library to make REST calls and retrive response details. This is wrapped around with by a library and framework build in java and maven to make writing tests, building and integrating with CI/CD systems easy. Cucumber-TestNG frameworks are also used. Cucumber makes test cases easy to maintain, create, read and understand to non-technical users while testNG is used to run the cucumber feature file and also makes it easy to integrate and scale with other systems.

![](https://bitbucket.org/rohanganapathypattada/gavagairepoimages/raw/d11e6563ffcb0b7fbf91b4a6f8c4ba46a5ce5064/gagavagai-explorer-restassured-tests-images/Cucumberfeature.png)

## Installation

OS X & Linux:

Install Brew
> 
> ```
> /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
> ```
	
Install Java

> ```
>  brew install java
> ```

Install Maven

> ```
> brew install maven
> ```
	
Windows:

Follow the steps to install Java and Maven: [Link](https://howtodoinjava.com/maven/how-to-install-maven-on-windows/)

## Usage example

To run the test from terminal

> ```
> mvn test
> ```
	
To clean files and directories generated by maven before the running the tests

> ```
> mvn clean test
> ```
	
Note: The tests run in the background, give it time to finish and generate the report.

## Reporting

A Cucumber report is generated after each test run

The report is located in target folder once the test has completed: gavagai-explorer-restassured-tests/target/cucumber-html-report/cucumber-html-reports/overview-features.html

The overview-features.html is the report overview

![](https://bitbucket.org/rohanganapathypattada/gavagairepoimages/raw/5038a40bb7a4ab6150e53097621956b7de4a81af/gagavagai-explorer-restassured-tests-images/reportoverview.png)

The report-feature_gavagaiAPI-feature.html can be accessed by clicking on the Feature name in the report overview. This shows all the test scenarios and test steps that are run as part of the test and also shows fail cases along with appropriate errors.

![](https://bitbucket.org/rohanganapathypattada/gavagairepoimages/raw/5038a40bb7a4ab6150e53097621956b7de4a81af/gagavagai-explorer-restassured-tests-images/reportoverviewfeature.png)

## Meta

Author – [@RohanGanapathy](rohanganapathy@gmail.com)

[bitbucket](https://bitbucket.org/%7B4674caf3-36f1-4934-9c9e-fb4768e30707%7D/)


[java-image]: https://img.shields.io/badge/java-1.7-orange
[java-url]: https://www.oracle.com/java/technologies/javase-jdk13-downloads.html
[mvn-image]: https://img.shields.io/badge/mvn-3.6.3-yellow
[mvn-url]: https://github.com/apache/maven
[cucumber-image]: https://img.shields.io/badge/cucumber-green
[cucumber-url]: https://github.com/cucumber/cucumber-jvm
[testng-image]: https://img.shields.io/badge/testng-darkgreen
[testng-url]: https://github.com/cucumber/cucumber-jvm/tree/master/testng
[restassured-image]: https://img.shields.io/badge/REST_assured-blue
[restassured-url]: https://github.com/rest-assured/rest-assured